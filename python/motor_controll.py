from pilot_vars import *
import sys

def thr(percent):
    thr_value = (((percent+100)*(limit_up-limit_down))/200)+limit_down
    pwm.set_pwm(t_stick, 0, thr_value)
    #print(thr_value)
    #sys.stdout.write("\r %s"%(thr_value))
    #sys.stdout.flush()

def aile(percent):
    aile_value = (((percent+100)*(limit_up-limit_down))/200)+limit_down
    pwm.set_pwm(a_stick, 0, aile_value)
    #print(aile_value)

def elev(percent):
    elev_value = (((percent+100)*(limit_up-limit_down))/200)+limit_down
    pwm.set_pwm(e_stick, 0, elev_value)
    #print(elev_value)

def rudd(percent):
    rudd_value = (((percent+100)*(limit_up-limit_down))/200)+limit_down
    pwm.set_pwm(r_stick, 0, rudd_value)
    #print(rudd_value)

def init():
    pwm.set_pwm(a_stick, 0, stick_center)
    pwm.set_pwm(e_stick, 0, stick_center)
    pwm.set_pwm(t_stick, 0, stick_center)
    pwm.set_pwm(r_stick, 0, stick_center)
