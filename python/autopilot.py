from pilot_vars import *
from motor_controll import *
from flightmode_settings import *
from pilot_socket import *
from gps import *
from baroSensor import *
import numpy as np

def takeoff(altAfterTakeoff):
    #try:
    global takeoff_gc
    global takeoff_alt

    current_gc = get_gc()
    while True:
        if isinstance(current_gc, float):
            break
        else:
            current_gc = get_gc()
    takeoff_gc = current_gc

    takeoff_alt_list = []
    for alt_measurement in range(10):
        takeoff_alt_list.append(bmp.readAltitude())
        time.sleep(1)

    takeoff_alt = np.mean(takeoff_alt_list)

    print("WARNING: TakeOFF!!!")
    time.sleep(1)
    arm("arm")
    if altAfterTakeoff > 100: #check userinput!
        print("ERROR: Variable altAfterTakeoff was over 100cm: %s"%altAfterTakeoff)
        arm("disarm")
        return False
    else:
        while True:
            current_gc = get_gc()
            if isinstance(current_gc, float):
                if current_gc < altAfterTakeoff: #Check for NONE!!
                    time.sleep(0.3)
                    print("Trying Takeoff to %s cm // Current GC: %s cm"%(altAfterTakeoff, get_gc()))
                    thr(40)
                else:
                    break
            else:
                print("Error: GET_GC Returned: %s"%current_gc)
                pass
        return takeoff_alt
    #except:
    #    return False
    #    print("ERROR: TAKEOFF() Failed")


def adjust_alt(target_alt, current_alt, current_gc):
    #try:
    #current_alt = (bmp.readAltitude() - takeoff_alt)
    #current_gc = get_gc()
    #print("Current GC: %s cm"%current_gc)
    if current_alt > 0 and current_alt < target_alt + 100:#Altitude over 0 and below targed+100cm
        if current_alt < target_alt - alt_diviation_max:
            thr(40)
            return (current_alt-target_alt) #Return Deviation

        elif current_gc > target_alt + alt_diviation_max:
            thr(-5)
            return (current_gc-target_alt) #Return Deviation
        else:
            thr(0)
    else:
        print("Error: ReadAltitude was %s" %current_alt)
        thr(-4)



def land(current_alt, current_gc, current_alt_status):
    if current_alt_status == False:
        if current_gc < 30:
            for i in range(10):
                gc_check_list1.append(get_gc())
            if np.mean(gc_check_list1) < 20:
                print("Warning: Landing altitude overridden! ALT: %s cm // GC: %s cm // GC Mean: %s cm"%(current_alt, current_gc, np.mean(gc_check_list1)))
                return True #disarm!
            else:
                thr(-5) #go down!
        else:
            thr(-5) #go down!
    elif current_alt_status == True:
        print("Current ALT: %s cm // Current GC: %s cm"%(current_alt,current_gc))
        if current_alt < 100: # in US-Sensor Range:
            if current_gc > takeoff_gc: #landing
                thr(-5)
            elif current_gc < takeoff_gc:   #landing finished #Double Check !!!!!
                for i in range(10):
                    gc_check_list2.append(get_gc())
                if np.mean(gc_check_list2) < 30:
                    return True
                else:
                    thr(-5)
        if current_alt >= 100: #not in US-Sensor Range
            thr(-7)



def adjust_dir(target_dir):
    try:
        current_dir = get_direction()
        print("Current Dir: %s"%(current_dir))
        #print("Diff: %s"%diff)
        diff = (target_dir-current_dir) % 360
        if diff < 180:
            #print("turn_right: %s"%(diff))
            rudd(20)#right
        else:
            #print("turn_left: %s"%(diff-180))
            rudd(-20)#left

        return target_dir - current_dir #Return Deviation

    except:
        rudd(0)
        print("Error: adjust_dir failed!!!")
        return False

def adjust_forward(speed):
    try:
        elev(speed)
    except:
        elev(0)
