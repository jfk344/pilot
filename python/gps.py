import serial
import struct
import math
import numpy as np

ser = serial.Serial("/dev/ttyS0", 115200, timeout=2)

msgid = {
    0x10 :  "GPS",
    0x20 :  "MAG"
}

msgid_inv = {
    "GPS"   :   0x10,
    "MAG"   :   0x20
}

fixtype =   {
    0x00    :   "No Fix",
    0x02    :   "2D Lock",
    0x03    :   "3D Lock"
}

def ParseDateTime(data):
    seconds =   (data & 0x3F        )   >>  0
    minutes =   (data & 0xFC0       )   >>  6
    hours   =   (data & 0xF000      )   >>  12
    day     =   (data & 0x1F0000    )   >>  16
    month   =   (data & 0x1E00000   )   >>  21
    year    =   (data & 0xFE000000  )   >>  25
    return {"seconds":seconds,"minutes":minutes,"hours":hours,"day":day,"month":month,"year":year}

def bin(s):
    return str(s) if s<=1 else bin(s>>1) + str(s&1)

def GenMagMask(b9):
    b9b =   bin(b9).rjust(8,"0")
    _0  =   int(b9b[7])
    _1  =   int(b9b[6])
    _2  =   int(b9b[5])
    _3  =   int(b9b[4])
    _4  =   int(b9b[3])
    _5  =   int(b9b[2])
    _6  =   int(b9b[1])
    _7  =   int(b9b[0])
    mask        =   [0,0,0,0,0,0,0,0]
    mask[7]     =   _0  ^   _4
    mask[6]     =   _1  ^   _5
    mask[5]     =   _2  ^   _6
    mask[4]     =   _3  ^   _7  ^   _0
    mask[3]     =   _1
    mask[2]     =   _2
    mask[1]     =   _3
    mask[0]     =   _4  ^   _0
    mask = int("".join([str(x) for x in mask]), 2)
    return mask

def get_long():
    try:
        return long
    except:
        print("ERROR: GET_LONG()")
        return False

def get_lat():
    try:
        return lat
    except:
        print("ERROR: GET_LAT()")
        return False

def get_alt():
    try:
        return alt
    except:
        print("ERROR: GET_ALT()")
        return False

def get_numSat():
    try:
        return SatNumber
    except:
        print("ERROR: GET_NUMSAT()")
        return False

def get_direction():
    try:
        return round(direction,2)
    except:
        print("ERROR: GET_Direction()")
        return False

def get_acc():
    try:
        return round(acc,2)
    except:
        print("ERROR: GET_Direction()")
        return False

def startGPS(run):
    global lat
    global long
    global alt
    global acc

    global SatNumber
    global direction

    while True:
        buff = ser.read(2)
        if buff == "\x55\xAA":
            #print "Received message! Decoding"
            id = ser.read(1)
            size   = ser.read(1)
            #print "Message size: %s Message ID: %s" %(hex(ord(size[0])),hex(ord(id[0])))
            buff = ser.read(ord(size[0]))
            #print "Message: %s" %buff
            checksum = ser.read(2)
            if  ord(id) == msgid_inv["GPS"]:
                xormask     =   struct.unpack("B", buff[55])[0]
                sequence    =   struct.unpack("H", buff[55:57])[0]
                numSat      =   struct.unpack("B", buff[48])[0]

                #print "Mask: %s" %xormask

                buff = bytearray(buff)
                for i in range(len(buff)):
                    buff[i] = buff[i] ^ xormask
                buff = str(buff)

                datetime    =   struct.unpack("I", buff[0:4])[0]

                #latitude    =   struct.unpack("i", buff[4:8])[0]
                #longitude   =   struct.unpack("i", buff[8:12])[0]
                longitude    =   struct.unpack("i", buff[4:8])[0]
                latitude     =   struct.unpack("i", buff[8:12])[0]

                altitude    =   struct.unpack("i", buff[12:16])[0]

                horzacc     =   struct.unpack("I", buff[16:20])[0]
                vertacc     =   struct.unpack("I", buff[20:24])[0]

                zero0       =   struct.unpack("I", buff[24:28])[0]

                nedNV       =   struct.unpack("I", buff[28:32])[0]
                nedEV       =   struct.unpack("I", buff[32:36])[0]
                nedDV       =   struct.unpack("I", buff[36:40])[0]

                posDOP      =   struct.unpack("H", buff[40:42])[0]
                verDOP      =   struct.unpack("H", buff[42:44])[0]
                norDOP      =   struct.unpack("H", buff[44:46])[0]
                easDOP      =   struct.unpack("H", buff[46:48])[0]

                zero1       =   struct.unpack("B", buff[49])[0]
                fixtype     =   struct.unpack("B", buff[50])[0]
                zero2       =   struct.unpack("B", buff[51])[0]
                fixstatus   =   struct.unpack("B", buff[52])[0]

                zero3       =   struct.unpack("H", buff[52:54])[0]


                #print "%s %s %s %s"%(latitude/1e7,longitude/1e7,altitude/1e7,numSat)
                #print datetime
                #print ParseDateTime(datetime)

                lat = latitude/1e7 #in degrees
                long = longitude/1e7 #in degrees
                alt = altitude/1e1 # in cm
                SatNumber = numSat
                acc = horzacc

            elif ord(id) == msgid_inv["MAG"]:
                buff    =   bytearray(buff)
                bmask       =   buff[4]
                xormask     =   GenMagMask(bmask)
                for i in range(len(buff)):
                    if i != 4:
                        buff[i] ^= xormask
                buff    =   str(buff)
                magx    =   struct.unpack("h", buff[0:2])[0]
                magy    =   struct.unpack("h", buff[2:4])[0]
                magz    =   struct.unpack("h", buff[4:6])[0]
                #print "Mag(%s,%s,%s)" %(magx,magy,magz)
                #        return(magx,magy,magz)
                headingNC = -np.arctan2(magy,magx)*180/3.1415 #von 0 bist - 180 rechts // von 0 - 180 links
                if headingNC < 0:
                    headingNC = headingNC + 360
                direction = headingNC

def get_distance(pointA, pointB):
    try:
        if (type(pointA) != tuple) or (type(pointB) != tuple):
            raise TypeError("Only tuples are supported as arguments")

        lat1 = (pointA[0])
        lat2 = (pointB[0])

        lon1 = (pointA[1])
        lon2 = (pointB[1])

        lat = (lat1 + lat2) / 2
        dx = 111.3 * (math.cos(lat)) * (lon2 - lon1)
        dy = 111.3 * (lat1 - lat2)

        distance = math.sqrt(dx * dx + dy * dy) * 1000
        return distance # in m
    except:
        print("ERROR: GET_Distance")
        return False

def get_bearing(pointA, pointB):
    try:
        if (type(pointA) != tuple) or (type(pointB) != tuple):
            raise TypeError("Only tuples are supported as arguments")

        lat1 = math.radians(pointA[0])
        lat2 = math.radians(pointB[0])

        diffLong = math.radians(pointB[1] - pointA[1])

        x = math.sin(diffLong) * math.cos(lat2)
        y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
                * math.cos(lat2) * math.cos(diffLong))

        initial_bearing = math.atan2(x, y)

        initial_bearing = math.degrees(initial_bearing)
        compass_bearing = (initial_bearing + 360) % 360

        return compass_bearing # in
    except:
        print("ERROR: GET:BEARING")
        return False
