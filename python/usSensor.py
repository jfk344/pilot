#Bibliotheken einbinden
import RPi.GPIO as GPIO
import time

#GPIO Modus (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#GPIO Pins zuweisen
GPIO_TRIGGER = 26
GPIO_ECHO = 16



#Richtung der GPIO-Pins festlegen (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)


def get_gc():
    old_distanz = 0
    sum_distanz = 0
    old_sum_distanz = 0
    correct_results = 0

    #Settings:
    runs = 8
    max_div = 20



    for i in range(1, runs, 1):
        # setze Trigger auf HIGH
        GPIO.output(GPIO_TRIGGER, True)

        # setze Trigger nach 0.01ms aus LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER, False)

        StartZeit = time.time()
        StopZeit = time.time()

        # speichere Startzeit
        while GPIO.input(GPIO_ECHO) == 0:
            StartZeit = time.time()

        # speichere Ankunftszeit
        while GPIO.input(GPIO_ECHO) == 1:
            StopZeit = time.time()

        # Zeit Differenz zwischen Start und Ankunft
        TimeElapsed = StopZeit - StartZeit
        # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
        # und durch 2 teilen, da hin und zurueck
        distanz = (TimeElapsed * 34300) / 2
        distanz = round(distanz,2)
        if(i == 1):
            old_distanz = distanz #start Distanz
            #print("first result: %s"%(old_distanz))
        else:
            if distanz > (old_distanz + max_div) or distanz < (old_distanz - max_div):
                #print("Messfehler: Distanz: %s // OLD Distanz: %s"%(distanz, old_distanz))
                pass
            else:
                correct_results = correct_results + 1 #korrektes Ergebniss gemessen
                sum_distanz = old_sum_distanz + distanz  #Neue Summe berechnen
                old_sum_distanz = sum_distanz  #Alte Summe ueberschreiben
                old_distanz = distanz #Alte Distanz ueberschreiben
                #print("Korrekte Messung: Distanz: %s // OLD Distanz: %s"%(distanz, (old_distanz)))

    #print("Ergbniss: %s // SUMME: %s // Korrekte Messungen %s "%((sum_distanz/correct_results), sum_distanz, correct_results))
    if correct_results == 0:
        pass
    else:
        return ((round(sum_distanz/correct_results, 2))-15)
