from flightmode_settings import *
from calibration_functions import *
from pilot_vars import *
from manual_flight import *
from motor_controll import *
from gps import *
from autopilot import *
from pilot_socket import *
from baroSensor import BMP085

import threading
import os
import sys
import getch
import time


#--- >>> GPS <<< ---
threadgps = threading.Thread(target=startGPS, args=(True,))
threadgps.start()

#DESTINATION:
#destination = (47.29626, 11.55843) #FELD
destination = (47.30002, 11.56492) #OMA

#setup_socket()

# --- Starting Procedere ----
try:
    print("INFO: Starting..." )
    print("Stick Center @"+str(stick_center))

    flightmode("GPS")
    ioc("OFF")
    init()
    print("INFO: Starting done!")
    time.sleep(2)

    while(get_numSat() < 5):
        sys.stdout.write("\rINFO: Found Sattelites: %s"%(get_numSat()))
        sys.stdout.flush()
    print("") #New Line
    print("INFO: Getting Homepoint...")
    time.sleep(10) 
    print("INFO: Found >= 5 Sattelites!")
    home_coordinates = (get_lat(),get_long(), bmp.readAltitude())
    print("INFO: CURRENT POS:  %s, %s  // %s "%(get_lat(),get_long(), bmp.readAltitude()))
    time.sleep(2)

    #waitforconnection()

# --------------------------------------------------------
except(KeyboardInterrupt):
    os.system('sudo pkill python')
    raise
# --- Starting Complete ---------------------------------------------------------------------------------

# -------------------------------- MAIN -----------------------------------------------------------------
try:

    takeoff_alt = takeoff(altAfterTakeoff)
    timeout = time.time() + 60*0.2 #Secounds

    while True:
        print("------------------------------------------------------------------------------------------------")
        if time.time() > timeout:
            break
        else:
            current_lat = get_lat() #Lattitude by GPS
            current_lon = get_long() #longitude by GPS
            current_alt = (bmp.readAltitude()-takeoff_alt) #Altitude by Barometer

            print("LAT: %s Degrees// LON: %s Degrees"%(current_lat,current_lon))
            print("ALT by Barometer: %s cm"%current_alt)

            alt_deviation = adjust_alt(flight_alt)


            time.sleep(0.05)

    # --- Landing -----
    while True:
        returnMessageLand = land()
        if returnMessageLand == False:
            print("ERROR: LANDING FAILED")
            thr(-10)
        elif returnMessageLand == True:
            print("INFO: Landing Finished")
            break
        time.sleep(0.5)
    os.system('sudo pkill python')


# --------- EXCEPT --------------------------------
except(KeyboardInterrupt):
    returnMessageLand = land()
    if returnMessageLand == False:
        print("ERROR: LANDING FAILED")
        thr(-100)
        os.system('sudo pkill python')
    else:
        os.system('sudo pkill python')
