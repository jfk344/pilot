import time
import Adafruit_PCA9685
from baroSensor import *
import socket

from usSensor import *

pwm = Adafruit_PCA9685.PCA9685(address=0x40)
pwm.set_pwm_freq(50) #50

bmp = BMP085(0x77, 3)

global stop_hover

ioc_switch = 5
mode_switch = 4
a_stick = 0
e_stick = 1
t_stick = 2
r_stick = 3

throttle_offset = 1

alt_drift_catch = 0

current_gc = 0
current_alt = 0

gc_check_list1 = []
gc_check_list2 = []

limit_up = 410
limit_down = 170
stick_center = (limit_up + limit_down)/2

engageLanding = False

onWayHome = False

max_deviation_gc=20
min_deviation_gc= -20

dir_deviation_max = 20


alt_diviation_max = 100

altAfterTakeoff = 60 #cm

flight_alt = 300 #cm

forward_speed = 20 #cm
