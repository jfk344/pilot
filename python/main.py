from flightmode_settings import *
from calibration_functions import *
from pilot_vars import *
from manual_flight import *
from motor_controll import *
from gps import *
from autopilot import *
from pilot_socket import *
from baroSensor import BMP085

import threading
import os
import sys
import getch
import time


#--- >>> GPS <<< ---
threadgps = threading.Thread(target=startGPS, args=(True,))
threadgps.start()

#DESTINATION:
#destination = (47.29626, 11.55843) #FELD
destination = (47.30002, 11.56492) #OMA


# --- Starting Procedere ----
try:
    print("INFO: Starting..." )
    print("Stick Center @"+str(stick_center))

    flightmode("GPS")
    ioc("OFF")
    init()
    print("INFO: Starting done!")
    time.sleep(2)

    #while(get_numSat() < 5):
    #    sys.stdout.write("\rINFO: Found Sattelites: %s"%(get_numSat()))
    #    sys.stdout.flush()
    print("") #New Line
    print("INFO: Getting Homepoint...")
    #time.sleep(10)
    print("INFO: Found >= 5 Sattelites!")
    home_coordinates = (get_lat(),get_long(), bmp.readAltitude())
    print("INFO: CURRENT POS:  %s, %s  // %s "%(get_lat(),get_long(), bmp.readAltitude()))
    time.sleep(2)

    #waitforconnection()

# --------------------------------------------------------
except(KeyboardInterrupt):
    #break
    os.system('sudo pkill python')
# --- Starting Complete ---------------------------------------------------------------------------------

# -------------------------------- MAIN -----------------------------------------------------------------
try:

    takeoff_alt = takeoff(altAfterTakeoff)
    timeout = time.time() + 60*0.04 #Secounds

    while True:
        print("------------------------------------------------------------------------------------------------")
        if time.time() > timeout:
            engageLanding = True

        # ------ GET Altitude by Barometer --------------------------------------------------------
        current_alt_unchecked = (bmp.readAltitude()-takeoff_alt) #Altitude by Barometer
        if current_alt_unchecked > 0 and current_alt_unchecked < (flight_alt + 100): # should be ok
            current_alt = current_alt_unchecked
            current_alt_status = True
            alt_drift_catch = 0
        else:
            current_alt_status = False
            if alt_drift_catch < 10:
                print("Error: Altitude Check detected: %s Drifts Chatched: %s"%(current_alt_unchecked,alt_drift_catch))
                alt_drift_catch += 1
            elif alt_drift_catch > 10:
                Print("ERROR: Altitude drift Catched! --> LANDING" )
                engageLanding = True
                #if Distance 2 home_coordinates > 10 m --> Failsafe ?

        # ------ GET GC by Ultrasonic Sensor ------------------------------------------------------
        current_gc_unchecked = get_gc()
        if isinstance(current_gc_unchecked,float) or isinstance(current_gc_unchecked,int):
            if current_gc_unchecked < 100 and current_gc_unchecked > -10:
                current_gc = current_gc_unchecked
                current_gc_status = True
            else:
                current_gc_status = False
        else:
            current_gc_status = False

        # -------------------------------------------------------------------------------------
        if engageLanding == True: # Landing:
            print("INFO: Landing")
            returnMessageLand = land(current_alt, current_gc, current_alt_status)
            if returnMessageLand == False:
                print("ERROR: LANDING FAILED")
                thr(-10)
            elif returnMessageLand == True:
                print("INFO: Landing Finished")
                break
            time.sleep(0.5)

        # -------------------------------------------------------------------------------------
        elif engageLanding == False: # fly
            current_lat = get_lat() #Lattitude by GPS
            current_lon = get_long() #longitude by GPS

            print("LAT: %s Degrees// LON: %s Degrees"%(current_lat,current_lon))
            print("ALT by Barometer: %s cm // Status: %s"%(current_alt, current_alt_status))

            alt_deviation = adjust_alt(flight_alt, current_alt, current_gc)

            targed_bearing = get_bearing((current_lat,current_lon),destination)
            print("Targed Bearing: %s"%targed_bearing)
            dir_deviation = adjust_dir(targed_bearing)

            distance2targed = get_distance((current_lat,current_lon),destination)
            print("Distance to targed: %s"%distance2targed)

            if distance2targed > 1: #When Targed more than 1m away:
                if (-alt_diviation_max <= alt_deviation <= alt_diviation_max) and (-dir_deviation_max <= dir_deviation <= dir_deviation_max):
                    adjust_forward(forward_speed)
                else:
                    print("Deviatons to big! GC_DEV: %s // DIR_DEV: %s"%(alt_deviation, dir_deviation))
                    adjust_forward(0)
            else: #When Targed within 1 m:
                if onWayHome == True:
                    engageLanding = True # Home Again !!!! -> LANDING!!
                else:
                    onWayHome = True
                    destination = home_coordinates
            #break
            time.sleep(0.05)

    os.system('sudo pkill python')


# --------- EXCEPT --------------------------------
except(KeyboardInterrupt):
        os.system('sudo pkill python')
