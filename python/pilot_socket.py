import socket

def setup_socket():
    global s
    port = 5000

    #--- >>> SOCKET <<< ---
    s = socket.socket()
    print("INFO: Socket successfully created")
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('', port))
    print("INFO: Socket binded to %s" %(port))
    s.listen(5)
    print("socket is listening")

def waitforconnection():
    global c
    c, addr = s.accept()
    print('Got connection from', addr)

def transmit(msg_Type, payload):
    try:
        c.send(str(msg_Type)+","+str(payload))
        # MSG TYPES: GC, DIR, LONG, LATT

    except:
        print("Error: Couldn't Transmit Payload")

def close_socket():
    c.close()
