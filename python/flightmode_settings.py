from pilot_vars import *
from motor_controll import *

#--------------- Set Flight Mode ------------------------
def flightmode(mode):
    if mode == "Failsafe":
        print("Fligtmode switched to Failsafe")
        pwm.set_pwm(mode_switch, 0, 290)

    if mode == "Atti":
        print("Fligtmode switched to Atti.")
        pwm.set_pwm(mode_switch, 0, 325)

    if mode == "GPS":
        print("Fligtmode switched to GPS")
        pwm.set_pwm(mode_switch, 0, 400)


# ---------- Set IOC MODE -----------------------------
def ioc(mode):
    if mode == "OFF":
        pwm.set_pwm(ioc_switch, 0, 400)

    if mode == "Home_Lock":
        pwm.set_pwm(ioc_switch, 0, 100)

    if mode == "Course_Lock":
        pwm.set_pwm(ioc_switch, 0, 320)



# ------- ARM or DISARM the Motors-------------------
def arm(mode):
    if mode == "arm":
        print("!!! arming !!!")

        pwm.set_pwm(e_stick, 0, limit_down)
        pwm.set_pwm(a_stick, 0, limit_up)
        pwm.set_pwm(t_stick, 0, limit_down)
        pwm.set_pwm(r_stick, 0, limit_up)
        time.sleep(1)
        init()
    if mode == "disarm":
        print("!!! disarming !!!")
        pwm.set_pwm(e_stick, 0, limit_down)
        pwm.set_pwm(a_stick, 0, limit_up)
        pwm.set_pwm(t_stick, 0, limit_down)
        pwm.set_pwm(r_stick, 0, limit_up)
        time.sleep(1)
        init()
