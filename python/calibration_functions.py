from pilot_vars import *

def calibrate_sticks():
    print("---Calibrating Sticks---")
    for c in range(3):
        for n in range(limit_down, limit_up, 2):
            pwm.set_pwm(a_stick, 0, n)
            pwm.set_pwm(e_stick, 0, n)
            pwm.set_pwm(t_stick, 0, n)
            pwm.set_pwm(r_stick, 0, n)
            time.sleep(0.01)
        print("reached"+str(limit_up))

        for i in range(limit_up, limit_down, -2):
            pwm.set_pwm(a_stick, 0, i)
            pwm.set_pwm(e_stick, 0, i)
            pwm.set_pwm(t_stick, 0, i)
            pwm.set_pwm(r_stick, 0, i)
            time.sleep(0.01)
        print("reached"+str(limit_down))

def calibrate_compass():
    for i in range(10):
        time.sleep(0.2)
        set_u("GPS")
        time.sleep(0.2)
        set_u("Atti")
