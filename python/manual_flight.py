from pilot_vars import *
from motor_controll import *
import click
from flightmode_settings import *


thr_value_up = 20
thr_value_down = -20

elev_value_forward = 20
elev_value_backward = -20

aile_value_left = 20
aile_value_right = -20

def manual_flight():
    click.echo('INOF: Manual Flight Enabled')
    while True:
        c = click.getchar()

# ----------- Controll -------------------------------------------
        if c == '.':
            thr(thr_value_up)
            time.sleep(1)
            thr(0)

        elif c == ',':
            thr(thr_value_down)
            time.sleep(1)
            thr(0)

        elif c == 'w':
            elev(elev_value_forward)
            time.sleep(1)
            elev(0)

        elif c == 's':
            elev(elev_value_backward)
            time.sleep(1)
            elev(0)

        elif c == 'a':
            aile(aile_value_left)
            time.sleep(1)
            aile(0)

        elif c == 'd':
            aile(aile_value_right)
            time.sleep(1)
            aile(0)

# ---------- Flight Settings -------------------------------------
        elif c == "1": #arming
            arm("arm")

        elif c == "0": #disarm
            arm("disarm")

        elif c == "g": #gps
            flightmode("GPS")

        else:
            click.echo('Invalid input :(')
